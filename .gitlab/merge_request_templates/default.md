
# Code Review Checklist
Checklist para realização do code review.

 :warning: **Atenção ao uso correto dos design patterns e a over-engineering**:warning:

 -- -

## Pull Request
- [x] A MR(Merge Request) não está grande.
- [x] A descrição da MR(Merge Request) está de acordo com o que o código executa.
- [x] Os commits estão com a mensagem no padrão especificado ([link](https://www.conventionalcommits.org/en/v1.0.0/))

## Implementação
- [x] O código está legível
- [x] Não existe código com intenção ambígua(DRY)
- [x] Não existe problema de lógica visível na implementação
- [x] O código obedece o design imposto no template padrão de projetos

## Tests
- [x] Existem testes unitários 

## Melhorias
- [x] Não há sugestão de melhoria 

## Observações:
(Adicionar observações)

