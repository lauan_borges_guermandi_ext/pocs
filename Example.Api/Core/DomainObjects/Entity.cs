﻿namespace Example.Api.Core.DomainObjects;

public abstract class Entity
{
    public Guid Id { get; init; }

    protected Entity()
        => Id = Guid.NewGuid();
}
