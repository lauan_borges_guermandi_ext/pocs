﻿namespace Example.Api.Core.Communication;

public record Event(string Type)
{
    public Guid Id { get; init; } = Guid.NewGuid();
    public EventStatus Status { get; set; } = EventStatus.Pending;
    public DateTimeOffset CreatedAt { get; init; } = DateTimeOffset.UtcNow;
}
