﻿namespace Example.Api.Core.Communication;

public enum EventStatus : ushort
{
    Pending = 0,
    Published = 1
}
