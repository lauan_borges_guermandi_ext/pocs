﻿namespace Example.Api.Core.Data;

public interface IRepository<T>
{
    IUnitOfWork UoW { get; }
}
