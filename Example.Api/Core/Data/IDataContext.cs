﻿using MongoDB.Driver;

namespace Example.Api.Core.Data;

public interface IDataContext
{
    Task<IClientSessionHandle> GetSession();
    IMongoCollection<T> GetCollection<T>(string name);
}
