﻿using Example.Api.Core.Communication;
using Example.Api.Domain.Aggregates;
using MongoDB.Driver;

namespace Example.Api.Core.Data;

public interface IUnitOfWork
{
    Task StartTransactionAsync();
    Task CommitAsync();
    Task RollbackAsync();
    IMongoCollection<Key> Keys { get; }
    IMongoCollection<Event> Events { get; }
}
