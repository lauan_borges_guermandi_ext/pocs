﻿using Example.Api.Core.Communication;

namespace Example.Api.Core.Adapters;

public interface IPubSubAdapter
{
    Task Publish(Event @event);
}
