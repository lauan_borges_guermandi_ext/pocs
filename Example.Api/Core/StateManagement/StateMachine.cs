﻿namespace Example.Api.Core.StateManagement;

public class StateMachine<TAction, TState>
    where TAction : struct, IConvertible
    where TState : struct, IConvertible
{
    private TState ConfigurableState { get; set; }
    public ICollection<StateRule<TAction, TState>> StateRules { get; private set; }

    public StateMachine()
        => StateRules = new List<StateRule<TAction, TState>>();

    public StateMachine<TAction, TState> FromState(TState configurableState)
    {
        ConfigurableState = configurableState;
        return this;
    }

    public StateMachine<TAction, TState> Permit(TAction trigger, TState state)
    {
        StateRules.Add(MakeStateRule(trigger, state));
        return this;
    }

    private StateRule<TAction, TState> MakeStateRule(TAction trigger, TState state)
        => new()
        {
            TargetState = state,
            Action = trigger,
            SourceState = ConfigurableState
        };
}