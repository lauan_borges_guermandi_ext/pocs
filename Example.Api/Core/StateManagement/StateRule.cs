﻿namespace Example.Api.Core.StateManagement;

public struct StateRule<TTrigger, TState>
{
    public TState SourceState { get; set; }
    public TState TargetState { get; set; }
    public TTrigger Action { get; set; }
}
