﻿using Example.Api.Domain.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Example.Api;

public static class Endpoints
{
    public static void MapAppEndpoints(this IEndpointRouteBuilder app)
    {
        app.MapPost("/key/register",
            (IMediator _mediator, [FromBody] RegisterKeyCommand command) => _mediator.Send(command))
            .Produces(200, null);

        app.MapPost("/key/request-portability",
            (IMediator _mediator, [FromBody] RequestKeyPortabilityCommand command) => _mediator.Send(command))
            .Produces(202, null);

        // Callback
        app.MapPut("/key/conclude-portability",
            (IMediator _mediator, [FromBody] ConcludePortabilityCommand command) => _mediator.Send(command))
            .Produces(200, null);
    }
}
