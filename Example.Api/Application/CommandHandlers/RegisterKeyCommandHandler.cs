﻿using AutoMapper;
using Example.Api.Core.Communication;
using Example.Api.Core.Data;
using Example.Api.Domain.Aggregates;
using Example.Api.Domain.Commands;
using Example.Api.Domain.Events;
using Example.Api.Infra.HttpClients;
using MediatR;

namespace Example.Api.Application.CommandHandlers;

public class RegisterKeyCommandHandler : IRequestHandler<RegisterKeyCommand>
{
    private readonly IMapper _mapper;
    private readonly IMediator _mediator;
    private readonly IPixExternalService _wiseConsClient;
    private readonly ILogger<RegisterKeyCommandHandler> _logger;
    private readonly IUnitOfWork _unitOfWork;

    public RegisterKeyCommandHandler(
        IMapper mapper,
        IMediator mediator,
        IPixExternalService wiseConsClient,
        ILogger<RegisterKeyCommandHandler> logger,
        IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _mediator = mediator;
        _wiseConsClient = wiseConsClient;
        _logger = logger;
        _unitOfWork = unitOfWork;
    }

    public async Task Handle(RegisterKeyCommand request, CancellationToken cancellationToken)
        => await _mapper.Map<Key>(request)
            .TryActionAsync(KeyAction.Register, (key) => RegisterKey(key));

    private async Task RegisterKey(Key key)
    {
        var response = await _wiseConsClient.RegisterPixKey(key)
            ?? throw new Exception("Error to register pix key."); // CodeSmell

        if (response.Unavailable)
            throw new Exception("This pix key needs portability."); // CodeSmell

        key.ExternalId = response.ExternalId;

        var @event = new KeyRegisteredEvent(key);
        await PersistKeyAndEvent(key, @event);
        await _mediator.Publish(@event);

        _logger.LogInformation("Key registred: {Id}", key.Id);


    }

    private async Task PersistKeyAndEvent(Key key, Event @event)
    {
        await _unitOfWork.StartTransactionAsync();
        await _unitOfWork.Keys.InsertOneAsync(key);
        await _unitOfWork.Events.InsertOneAsync(@event);
        await _unitOfWork.CommitAsync();
    }
}

