﻿using Example.Api.Core.Communication;
using Example.Api.Core.Data;
using Example.Api.Domain.Aggregates;
using Example.Api.Domain.Commands;
using Example.Api.Domain.Events;
using MediatR;
using MongoDB.Driver;

namespace Example.Api.Application.CommandHandlers;

public class ConcludePortabilityCommandHandler : IRequestHandler<ConcludePortabilityCommand>
{
    private readonly IMediator _mediator;
    private readonly ILogger<ConcludePortabilityCommandHandler> _logger;
    private readonly IUnitOfWork _unitOfWork;

    public ConcludePortabilityCommandHandler(
        IMediator mediator,
        ILogger<ConcludePortabilityCommandHandler> logger,
        IUnitOfWork unitOfWork)
    {
        _mediator = mediator;
        _logger = logger;
        _unitOfWork = unitOfWork;
    }

    public async Task Handle(ConcludePortabilityCommand request, CancellationToken cancellationToken)
    {
        var result = await _unitOfWork.Keys.FindAsync(
            Builders<Key>.Filter.Eq(nameof(Key.ExternalId), request.ExternalId), cancellationToken: cancellationToken);

        var key = result.SingleOrDefault(cancellationToken)
            ?? throw new Exception("Key doesn't exists."); // CodeSmell

        await key.TryActionAsync(KeyAction.ConcludePortability, (key) => ConcludePortability(key));
    }

    public async Task ConcludePortability(Key key)
    {
        await _unitOfWork.Keys.ReplaceOneAsync(
            Builders<Key>.Filter.Eq(nameof(key.ExternalId), key.ExternalId), key);

        var @event = new KeyRegisteredEvent(key);
        await PersistKeyAndEvent(key, @event);
        await _mediator.Publish(@event);

        _logger.LogInformation("Key registred: {Id}", key.Id);
    }

    private async Task PersistKeyAndEvent(Key key, Event @event)
    {
        await _unitOfWork.StartTransactionAsync();
        await _unitOfWork.Keys.ReplaceOneAsync(Builders<Key>.Filter.Where(x => x.Id == key.Id), key);
        await _unitOfWork.Events.InsertOneAsync(@event);
        await _unitOfWork.CommitAsync();
    }
}
