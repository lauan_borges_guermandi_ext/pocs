﻿using AutoMapper;
using Example.Api.Core.Communication;
using Example.Api.Core.Data;
using Example.Api.Domain.Aggregates;
using Example.Api.Domain.Commands;
using Example.Api.Domain.Events;
using Example.Api.Infra.HttpClients;
using MediatR;

namespace Example.Api.Application.CommandHandlers;

public class RequestKeyPortabilityCommandHandler : IRequestHandler<RequestKeyPortabilityCommand>
{
    private readonly IMapper _mapper;
    private readonly IMediator _mediator;
    private readonly IPixExternalService _wiseConsClient;
    private readonly ILogger<RequestKeyPortabilityCommandHandler> _logger;
    private readonly IUnitOfWork _unitOfWork;

    public RequestKeyPortabilityCommandHandler(
        IMapper mapper,
        IMediator mediator,
        IPixExternalService wiseConsClient,
        ILogger<RequestKeyPortabilityCommandHandler> logger,
        IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _mediator = mediator;
        _wiseConsClient = wiseConsClient;
        _logger = logger;
        _unitOfWork = unitOfWork;
    }

    public async Task Handle(RequestKeyPortabilityCommand request, CancellationToken cancellationToken)
        => await _mapper.Map<Key>(request)
            .TryActionAsync(
                KeyAction.RequestPortability, (key) => RequestKeyPortability(key, request.ReinvidicationType));

    public async Task RequestKeyPortability(Key key, KeyReinvidicationType type)
    {
        var response = await _wiseConsClient.RequestKeyPortability(key, type)
            ?? throw new Exception("Error to request pix key portability."); // CodeSmell: Use a specialized exception

        key.ExternalId = response.ExternalId;

        var @event = new KeyWaitingPortabilityEvent(key);
        await PersistKeyAndEvent(key, @event);
        await _mediator.Publish(@event);

        _logger.LogInformation("Key waiting portability: {Id}", key.Id);
    }

    private async Task PersistKeyAndEvent(Key key, Event @event)
    {
        await _unitOfWork.StartTransactionAsync();
        await _unitOfWork.Keys.InsertOneAsync(key);
        await _unitOfWork.Events.InsertOneAsync(@event);
        await _unitOfWork.CommitAsync();
    }
}
