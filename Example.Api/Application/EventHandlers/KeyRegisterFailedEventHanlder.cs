﻿using Example.Api.Core.Adapters;
using Example.Api.Core.Communication;
using Example.Api.Core.Data;
using Example.Api.Domain.Events;
using MediatR;
using MongoDB.Driver;

namespace Example.Api.Application.EventHandlers;

public class KeyRegisterFailedEventHanlder : INotificationHandler<KeyRegisterFailedEvent>
{
    private readonly IPubSubAdapter _pubSubService;
    private readonly IUnitOfWork _unitOfWork;

    public KeyRegisterFailedEventHanlder(IPubSubAdapter pubSubService, IUnitOfWork unitOfWork)
    {
        _pubSubService = pubSubService;
        _unitOfWork = unitOfWork;
    }

    public async Task Handle(KeyRegisterFailedEvent @event, CancellationToken cancellationToken)
    {
        @event.Status = EventStatus.Published;

        await _pubSubService.Publish(@event);
        await _unitOfWork.Events.ReplaceOneAsync(
            Builders<Event>.Filter.Where(x => x.Id == @event.Id), @event, cancellationToken: cancellationToken);
    }
}
