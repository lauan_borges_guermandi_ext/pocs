﻿using Example.Api.Core.Adapters;
using Example.Api.Core.Communication;

namespace Example.Api.Infra.Bus;

public class PubSubService : IPubSubAdapter
{
    public async Task Publish(Event @event)
        => Console.WriteLine($"{@event.Type} is published on Pub/Sub .");
}
