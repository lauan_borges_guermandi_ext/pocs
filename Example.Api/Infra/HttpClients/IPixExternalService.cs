﻿using Example.Api.Domain.Aggregates;

namespace Example.Api.Infra.HttpClients;

public interface IPixExternalService
{
    Task<RequestKeyPortabilityResponse> RequestKeyPortability(Key key, KeyReinvidicationType type);
    Task<RegisterPixKeyResponse> RegisterPixKey(Key key);
}
