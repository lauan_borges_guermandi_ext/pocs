﻿using Example.Api.Domain.Aggregates;

namespace Example.Api.Infra.HttpClients;

// fake service
public class PixExternalService : IPixExternalService
{
    public async Task<RegisterPixKeyResponse> RegisterPixKey(Key key)
        => new(Guid.NewGuid());

    public async Task<RequestKeyPortabilityResponse> RequestKeyPortability(Key key, KeyReinvidicationType type)
        => new(Guid.NewGuid());
}

public record RegisterPixKeyResponse(Guid ExternalId, bool Unavailable = true) { }

public record RequestKeyPortabilityResponse(Guid ExternalId, bool WatingPortability = true) { }

