﻿using AutoMapper;
using Example.Api.Domain.Aggregates;
using Example.Api.Domain.Commands;

namespace Example.Api.Infra.Mappers;

public class MapCommandToEntity : Profile
{
    public MapCommandToEntity()
    {
        CreateMap<RegisterKeyCommand, Key>()
            .ConstructUsing(x => new Key(x.Key, x.Type));

        CreateMap<RequestKeyPortabilityCommand, Key>()
            .ConstructUsing(x => new Key(x.Key, x.Type));
    }
}
