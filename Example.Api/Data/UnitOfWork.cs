﻿using Example.Api.Core.Communication;
using Example.Api.Core.Data;
using Example.Api.Domain.Aggregates;
using MongoDB.Driver;

namespace Example.Api.Data;

public class UnitOfWork : IUnitOfWork, IDisposable
{
    private readonly IDataContext _context;
    private readonly ILogger<UnitOfWork> _logger;

    public IMongoCollection<Key> Keys { get; private set; }
    public IMongoCollection<Event> Events { get; private set; }

    private IClientSessionHandle Session { get; set; }

    public UnitOfWork(IDataContext context, ILogger<UnitOfWork> logger)
    {
        _context = context;
        _logger = logger;

        Keys = _context.GetCollection<Key>(nameof(Keys));
        Events = _context.GetCollection<Event>(nameof(Events));
    }

    public async Task StartTransactionAsync()
    {
        Session ??= await _context.GetSession();

        if (!Session.IsInTransaction)
            Session.StartTransaction();
    }

    public async Task CommitAsync()
    {
        try
        {
            await Session.CommitTransactionAsync();
        }
        catch (Exception ex)
        {
            Session.AbortTransaction();
            _logger.LogError(ex, "The transaction was aborted. {Message}", ex.Message);
        }
    }

    public async Task RollbackAsync()
        => await Session.AbortTransactionAsync();

    public void Dispose()
        => Session?.Dispose();
}
