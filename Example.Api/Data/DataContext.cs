﻿using Example.Api.Core.Data;
using MongoDB.Driver;

namespace Example.Api.Data;

public class DataContext : IDataContext
{
    private IMongoDatabase Database { get; set; }
    private MongoClient MongoClient { get; set; }

    public DataContext(IConfiguration configuration)
    {
        MongoClient = new MongoClient(configuration.GetConnectionString("DICT"));
        Database = MongoClient.GetDatabase("DICT");
    }

    public IMongoCollection<T> GetCollection<T>(string name)
        => Database.GetCollection<T>(name);

    public async Task<IClientSessionHandle> GetSession()
        => await MongoClient.StartSessionAsync();
}
