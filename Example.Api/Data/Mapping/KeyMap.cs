﻿using Example.Api.Data.Extensions;
using Example.Api.Domain.Aggregates;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;

namespace Example.Api.Data.Mapping;

public class KeyMap
{
    public static void Configure()
        => BsonClassMapExtensions.RegisterIfNotCreated<Key>(map =>
        {
            map.AutoMap();
            map.SetIgnoreExtraElements(true);

            map.MapMember(x => x.ExternalId)
              .SetIsRequired(true)
              .SetSerializer(new GuidSerializer(BsonType.String));

            map.MapMember(x => x.Value)
               .SetIsRequired(true)
               .SetSerializer(new StringSerializer(BsonType.String));

            map.MapMember(x => x.Type)
               .SetIsRequired(true)
               .SetSerializer(new EnumSerializer<KeyType>(BsonType.String));

            map.MapMember(x => x.CurrentState)
               .SetIsRequired(true)
               .SetSerializer(new EnumSerializer<KeyState>(BsonType.String));
        });
}
