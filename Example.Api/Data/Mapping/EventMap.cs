﻿using Example.Api.Core.Communication;
using Example.Api.Data.Extensions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;

namespace Example.Api.Data.Mapping;

public static class EventMap
{
    public static void Configure() => BsonClassMapExtensions.RegisterIfNotCreated<Event>(map =>
    {
        map.AutoMap();
        map.SetIgnoreExtraElements(true);

        map.MapProperty(x => x.Id)
            .SetSerializer(new GuidSerializer(BsonType.String));

        map.MapProperty(x => x.CreatedAt)
            .SetIsRequired(true)
            .SetSerializer(new DateTimeOffsetSerializer(BsonType.String));

        map.MapProperty(x => x.Type)
            .SetIsRequired(true)
            .SetSerializer(new StringSerializer(BsonType.String));

        map.MapProperty(x => x.Status)
            .SetIsRequired(true)
            .SetSerializer(new EnumSerializer<EventStatus>(BsonType.String));
    });
}
