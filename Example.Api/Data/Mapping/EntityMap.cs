﻿using Example.Api.Core.DomainObjects;
using Example.Api.Data.Extensions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;

namespace Example.Api.Data.Mapping;

public static class EntityMap
{
    public static void Configure() => BsonClassMapExtensions.RegisterIfNotCreated<Entity>(map =>
    {
        map.AutoMap();
        map.SetIgnoreExtraElements(true);

        map.MapProperty(x => x.Id)
            .SetSerializer(new GuidSerializer(BsonType.String));
    });
}
