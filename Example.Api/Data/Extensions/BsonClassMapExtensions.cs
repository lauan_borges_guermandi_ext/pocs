﻿using MongoDB.Bson.Serialization;

namespace Example.Api.Data.Extensions;

public static class BsonClassMapExtensions
{
    public static void RegisterIfNotCreated<T>(Action<BsonClassMap<T>> classMapInitializer)
    where T : class
    {
        var isRegistered = BsonClassMap.IsClassMapRegistered(typeof(T));
        if (!isRegistered)
        {
            BsonClassMap.RegisterClassMap(classMapInitializer);
        }
    }

}
