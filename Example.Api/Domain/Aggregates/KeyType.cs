﻿namespace Example.Api.Domain.Aggregates;

public enum KeyType : uint
{
    RandomKey,
    Cpf,
    Cnpj,
    Email,
    Phone
}
