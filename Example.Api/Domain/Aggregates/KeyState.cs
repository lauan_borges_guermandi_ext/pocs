﻿namespace Example.Api.Domain.Aggregates;

public enum KeyState : uint
{
    Initialized,
    Registered,
    WaitingPortability,
    PortabilityDenied,
    WaitingPossession,
    PossessionDenied
}
