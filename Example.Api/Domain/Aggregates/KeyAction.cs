﻿namespace Example.Api.Domain.Aggregates;

public enum KeyAction : uint
{
    Register,
    RequestPortability,
    ConcludePortability,
    DenyPortability,
    RequestPossession,
    ConcludePossession,
    DenyPossession
}
