﻿namespace Example.Api.Domain.Aggregates;

public enum KeyReinvidicationType
{
    Portability,
    Possession
}
