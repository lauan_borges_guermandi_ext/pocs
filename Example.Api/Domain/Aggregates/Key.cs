﻿using Example.Api.Core.DomainObjects;
using Example.Api.Domain.StateManagement;

namespace Example.Api.Domain.Aggregates;

public class Key : Entity
{
    public Guid ExternalId { get; set; }
    public string Value { get; init; }
    public KeyType Type { get; init; }

    public KeyState CurrentState
    {
        get => _stateMachine.CurrentState;
        init => _stateMachine = new(value);
    }

    private KeyStateMachine _stateMachine = new();

    public Key(string value, KeyType type)
    {
        Value = value;
        Type = type;

        // Validate here
    }

    public async Task TryActionAsync(KeyAction action, Func<Key, Task> actionHandler)
    {
        if (!_stateMachine.CanExecute(action))
            throw new Exception($"You can't execute action {Enum.GetName(action)}."); // CodeSmell

        try
        {
            _stateMachine.ChangeStateByAction(action);
            await actionHandler.Invoke(this);
        }
        catch
        {
            _stateMachine.ResetState();
            throw;
        }
    }
}
