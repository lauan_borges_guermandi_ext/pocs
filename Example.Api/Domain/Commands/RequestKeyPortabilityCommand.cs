﻿using Example.Api.Domain.Aggregates;
using MediatR;

namespace Example.Api.Domain.Commands;

public record RequestKeyPortabilityCommand(
    KeyReinvidicationType ReinvidicationType, KeyType Type, string Key) : IRequest;
