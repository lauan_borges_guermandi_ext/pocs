﻿using Example.Api.Domain.Aggregates;
using MediatR;

namespace Example.Api.Domain.Commands;

public record RegisterKeyCommand(KeyType Type, string Key) : IRequest;
