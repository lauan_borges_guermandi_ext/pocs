﻿using MediatR;

namespace Example.Api.Domain.Commands;

public record ConcludePortabilityCommand(Guid ExternalId) : IRequest;
