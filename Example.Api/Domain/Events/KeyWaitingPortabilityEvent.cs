﻿using Example.Api.Core.Communication;
using Example.Api.Domain.Aggregates;
using MediatR;

namespace Example.Api.Domain.Events;

public record KeyWaitingPortabilityEvent(Key Key)
    : Event(nameof(KeyWaitingPortabilityEvent)), INotification;
