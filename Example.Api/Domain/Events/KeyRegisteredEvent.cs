﻿using Example.Api.Core.Communication;
using Example.Api.Domain.Aggregates;
using MediatR;

namespace Example.Api.Domain.Events;

public record KeyRegisteredEvent(Key Key)
    : Event(nameof(KeyRegisteredEvent)), INotification;
