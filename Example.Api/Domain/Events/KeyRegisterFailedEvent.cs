﻿using Example.Api.Core.Communication;
using Example.Api.Domain.Aggregates;
using MediatR;

namespace Example.Api.Domain.Events;

public record KeyRegisterFailedEvent(Key Key, string Message)
    : Event(nameof(KeyRegisterFailedEvent)), INotification;
