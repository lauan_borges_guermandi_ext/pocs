﻿using Example.Api.Core.StateManagement;
using Example.Api.Domain.Aggregates;

namespace Example.Api.Domain.StateManagement;

public class KeyStateMachine
{
    private readonly StateMachine<KeyAction, KeyState> _stateMachine;

    public KeyState PreviousState { get; private set; }
    public KeyState CurrentState { get; private set; } = KeyState.Initialized;

    public KeyStateMachine()
    {
        _stateMachine = new();
        ConfigureStateMachine();
    }

    public KeyStateMachine(KeyState initialState)
    {
        CurrentState = initialState;

        _stateMachine = new();
        ConfigureStateMachine();
    }

    private void ConfigureStateMachine()
    {
        _stateMachine.FromState(KeyState.Initialized)
            .Permit(KeyAction.Register, KeyState.Registered)
            .Permit(KeyAction.RequestPortability, KeyState.WaitingPortability)
            .Permit(KeyAction.RequestPossession, KeyState.WaitingPossession);

        _stateMachine.FromState(KeyState.WaitingPortability)
            .Permit(KeyAction.ConcludePortability, KeyState.Registered)
            .Permit(KeyAction.DenyPortability, KeyState.PortabilityDenied);

        _stateMachine.FromState(KeyState.WaitingPossession)
            .Permit(KeyAction.ConcludePossession, KeyState.Registered)
            .Permit(KeyAction.DenyPossession, KeyState.PossessionDenied);
    }

    public bool CanExecute(KeyAction action)
        => _stateMachine.StateRules
            .Any(x => x.SourceState == CurrentState
                && x.Action == action);

    public void ChangeStateByAction(KeyAction action)
    {
        if (!CanExecute(action))
            throw new Exception($"You can't execute this action({action}).");  // CodeSmell: Use a specialized exception

        var stateRule = _stateMachine.StateRules
                .FirstOrDefault(x => x.SourceState == CurrentState
                    && x.Action == action);

        Console.WriteLine($"Trasitioning from {CurrentState} to {stateRule.TargetState} triggered by {Enum.GetName(action)}.");
        PreviousState = CurrentState;
        CurrentState = stateRule.TargetState;
    }

    public void ResetState()
        => CurrentState = PreviousState;
}
