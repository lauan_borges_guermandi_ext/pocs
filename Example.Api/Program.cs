using Example.Api;
using Example.Api.Core.Adapters;
using Example.Api.Core.Data;
using Example.Api.Data;
using Example.Api.Data.Mapping;
using Example.Api.Infra.Bus;
using Example.Api.Infra.HttpClients;
using Example.Api.Infra.Mappers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAutoMapper(x => x.AddProfile<MapCommandToEntity>());

builder.Services.AddMediatR(x => x.RegisterServicesFromAssemblyContaining<Program>());

builder.Services.AddScoped<IDataContext, DataContext>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

builder.Services.AddScoped<IPubSubAdapter, PubSubService>();

builder.Services.AddScoped<IPixExternalService, PixExternalService>();

BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));

EntityMap.Configure();
KeyMap.Configure();
EventMap.Configure();

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();

app.MapAppEndpoints();

app.Run();
